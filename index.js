
    //Requires firebase node package '$ npm install firebase --save'

    var firebase = require ("firebase");

    var config = {
        apiKey: "AIzaSyB-SJS11gTv5xn4aM8Xze8z1kvr4Ad2sBo",
        authDomain: "message-media-ar-chat.firebaseapp.com",
        databaseURL: "https://message-media-ar-chat.firebaseio.com",
        projectId: "message-media-ar-chat",
        storageBucket: "message-media-ar-chat.appspot.com",
        messagingSenderId: "40003784917"
    };

    firebase.initializeApp(config);


    var database = firebase.database();

    /*

    COMMENTED OUT TRIGGER CODE FROM MESSAGE MEDIA

    exports.handler = function(event, context, callback) {
        let input = JSON.parse(event.body);
        */
    
    //message struct coming from message media    
    
    let message = {

       author : "Ian",
       content : "Iam nintendo man", //when notifiying user, if we pass their name in meta data, we can get it here, and use it to update the thread, otherwise would require some fancy look ups
       source_number  : "0451039730", //in this instance we wouldn't need their number any more, as we've already got it, and they are subsrcibed to the thread, but firebase expects some value, even if null
       thread_id : "144958_Message",
        
        /*

        LIKELY MESSAGE MEDIA STRUCT
        author : input.author, 
        content : input.content,
        source_number : input.source_number, // 
        thread_id : input.metadata.thread_id // we use this to define the thread we're updating, this also needs to get passed in the notifcation 
        */

    };


    //call back function that first counts number of messages in thread
    database.ref('threads/' + message.thread_id +'/messages/').once('value', function(snapshot){
        
        var count = snapshot.numChildren();

        //uses this value to set the name of the message object (unity list objects have a value, which become a name in firebase)
        //creates a record using this name, and the message struct set above
        database.ref('threads/' + message.thread_id +'/messages/' + count ).set({

        //
        author : message.author,
        content : message.content,
        phoneNumber: message.source_number,

      });

        //increases the message count in the corresponding object, which triggers update client side (game object and data are seperate tables as firebase preffers flat heirarchy)
        database.ref('objects/' + message.thread_id +'/MessageCount/').set(count);

    } );


    /*NOTE    
     - Optionally send notifictaion to previous message sender
     - Possibly use a central function (possibly here) that manages this for both unity and web side
     - May cause threads to go out of sync as manually setting message number */
